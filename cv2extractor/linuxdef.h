/*---------------------------------------------------------------------------
 PROJET     : Couche de portabilite multi-systeme
 
 PLATE-FORME: LINUX
 
 MODULE     : Definition des macros et des types utilises par la couche 
              d'abstraction systeme pour la plate-forme LINUX.
              
              Le fichier de definition des types portable (ce fichier) doit 
              etre inclus avant tout autre .h de definitions d'APIS gip-cps. 
              Ce fichier definit tous les types et macros dependants du 
              systeme. 

 FICHIER    : linuxdef.h

 VERSION    : 1.01
 
 DATE       : 6/06/2008
 
 AUTEUR     : ALVARO ROCHA    modifications : Charles Puissant (GFI)
 
-----------------------------------------------------------------------------
 Modifications:  (nouvelle version, date, auteur, explication)
 ...
 24/08/2010 : GFI
	Remplacement du #define d'inclusion linuxdef.h __CPSCASDF_H par __LINUXDF_H
-----------------------------------------------------------------------------
---------------------------------------------------------------------------*/


#ifndef __LINUXDF_H
#define __LINUXDF_H

/*----------------- definition des macros portables ------------------------*/

#define far
#define FAR

#define PTR *

#define PVOID void PTR
#define FARPTR PTR
#define FPVOID void FARPTR

/* AROC 12/02/2001 debut : */
#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE 1
#endif
/* AROC 12/02/2001 fin */

#ifndef NULL
#define NULL ((void *)0)
#endif

/*----------------- definition des types portables ------------------------*/

typedef char CHAR;
typedef CHAR PTR PCHAR;

typedef CHAR PTR PSTRG; 

#ifndef __wintypes_h__
typedef unsigned char UCHAR;
typedef UCHAR PTR PUCHAR;
#endif

typedef char INT8;
typedef short INT16;
typedef long INT32;

typedef INT8 PTR PINT8;
typedef INT16 PTR PINT16;
typedef INT32 PTR PINT32;

typedef unsigned char UINT8;
typedef unsigned short UINT16;
typedef unsigned long UINT32;

typedef UINT8 PTR PUINT8;
typedef UINT16 PTR PUINT16;
typedef UINT32 PTR PUINT32;

/* 11/02/1997 : Ajout DRE pour CPSCAS */
typedef void VOID;
typedef char PTR PSTR;
/* 11/02/1997 : Fin */

typedef long LONG;
typedef char far PTR LPSTR;

#ifndef __wintypes_h__
typedef unsigned short WORD;
typedef unsigned long DWORD;
typedef unsigned char BYTE;
typedef BYTE far * LPBYTE;

#ifndef BOOL
	typedef short int BOOL;
#endif

#endif

#define Unref( a) a=a

#ifndef min
#define min(a,b) ((a)>(b)?(b):(a))
#endif

#ifndef max
#define max(a,b) ((a)<(b)?(b):(a))
#endif

/* Taille maximale allouée par la fonction "malloc" */
#ifndef MAXBLOC
#define MAXBLOC     4294967295
#endif

#ifndef API_ENTRY
#define API_ENTRY
#endif


#endif


