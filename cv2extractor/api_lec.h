/*
%-------------------------------------------------------------------------------------------------
% PROJET    : API de Lecture
%
% MODULE    : APILEC
%
% FICHIER   : Api_lec.h
%
%	D�finitions communes � l'ensemble des fonctionnalit�s des API de lecture
%
%-------------------------------------------------------------------------------------------------
*/

#ifndef __LEC_H_VU__
#define __LEC_H_VU__

#ifdef WIN32
    #include "win32def.h"
#else
    #ifdef TARGET_API_MAC_OSX
		#include "osxdef.h"
    #else
		#ifdef UNIX
			#include "linuxdef.h"
		#endif
    #endif
#endif

#ifdef IN
    #undef IN
#endif
#define IN		const

#ifndef OUT
	#define OUT
#endif

#ifndef IN_OUT
	#define IN_OUT
#endif

/*
--------------------------------------------------------------------------------------------------
*/

/* constantes relatives aux coupleurs */
#define COUPLEUR_ABSENT     0xFFFF
#define COUPLEUR_VITALE     0x0001
#define COUPLEUR_CPS        0x0002


/* Constantes CPS */

#define TYPE_CPS	"00"		/* Carte de type CPS */
#define TYPE_CPE	"01"		/* Carte de type CPE */
#define TYPE_CPF	"02"		/* Carte de type CPF */
#define TYPE_CDE	"03"		/* Carte de type CDE */

#define CPS_REELLE	"00"		/* CPS r�elle */
#define CPS_TEST	"80"		/* CPS de test */

#ifdef FULLPCSC
#define NOM_RESSOURCE_CPS	"CPS"
#else
#define NOM_RESSOURCE_CPS	"TRANSPA1"
#endif

/* Tables pour la fonction Dictionnaire */
#define TABLE_REGIME		1	/* R�gimes								*/
#define TABLE_CAISSE		2	/* Nom des caisses de rattachement		*/
#define TABLE_GESTION		3	/* Code Gestion							*/
#define TABLE_COUV			5	/* Libell� Code Couverture				*/
#define TABLE_QUALITE		6	/* Qualit� b�n�ficiaire					*/
#define TABLE_CIVILITE		7	/* Civilit� du PS						*/
#define TABLE_DIVERS		11	/* Table divers							*/
#define TABLE_DROIT			12	/* P�riode de droit AMO					*/
#define TABLE_CMU			13	/* CMU									*/
#define TABLE_CODEROUT		14	/* Code routage AMC						*/
#define TABLE_INDICAMC		15	/* Indicateur de traitement AMC			*/
#define TABLE_INDICMUT		16	/* Indicateur de traitement Mutuelle	*/
#define TABLE_PRJEURO		17	/* Activit� projet europ�en				*/
#define TABLE_SERVAMO		18	/* Services AMO							*/
#define TABLE_SERVMUTAMC	19	/* Services Mutuelle / AMC				*/
#define TABLE_STS			20	/* Code aiguillage STS					*/
#define TABLE_MEDECIN		21  /* Medecin traitant						*/


/* Type de Timer  */
#define TIMER_CONTROLE		1	/* Timer de controle de la CPS	*/
#define TIMER_INACTIVITE	2	/* Timer d'inactivit�			*/


/* Modes de fonctionnement */
#define FLIP_FLOP			1
#define CPS_EN_LIGNE		2


/* Etats de la CPS (ces �tats peuvent se combiner) */
#define CPS_RELLE			0x0000
#define CPS_ABSENTE			0x0001
#define CPS_BLOQUEE			0x0002
#define CPS_CODE_ACTIF		0x0004		/* Le code porteur a �t� pr�sent� */
#define CPS_CODE_INACTIF	0x0000
#define CPS_HORSVALIDITE	0x0008		/* La date de validit� est expir�e */
#define CPS_IDENTINCORRECT	0x0010		/* CPS avec identificateur d'application incorrect */
#define CPS_NONAUTORISEE	0x0020		/* Type de CPS non autoris�e (CPA) */
#define CPS_DETEST			0x0040

/* Etats de la carte VITALE */
#define CARTE_REELLE        0x0000
#define CARTE_INVALIDE		0x0001
#define CARTE_BLOQUEE		0x0002
#define CARTE_INCORRECTE	0x0003
#define CARTE_SATUREE		0x0004
#define CARTE_PERIMEE		0x0005
#define CARTE_TEST			0x1000
#define CARTE_DEMO			0x2000

/* �tats des coupleurs */
#define ETAT_CABSENTE       0x00
#define ETAT_CPRESENTE      0x01
#define ETAT_CSOUSTENSION   0x02

/* Informations concernant la famille */
#define LG_NIR             13            /* Longueur du NIR */
#define LG_CLE              2            /* Longueur de la cl� du NIR */

/* Informations concernant un b�n�ficiaire */
/*#define BEN_DRT_INT_NB  3       /* nombre d'intervalles de droits au r�gime obligatoire */

/* Mutuelle */
#define MUT_NUM_LG      8       /* longueur du num�ro de mutuelle          */
#define MUT_TYP_NB      8       /* nombre de cat�gories du contrat         */
#define MUT_TYP_LG      25      /* longueur du libell� d'une cat�gorie     */
#define MUT_INT_NB      3       /* nombre d'intervalles de droits mutuelle */

/* Taille maxi buffer Infos poste*/
#define	MAX_SIZE_INFOPOSTECPS	30000	/* longueur maxi admissible du buffer retour CPS_InformationPoste */

#define APILECTURE     "Api_Lec.dll"    /* Nom de l'API de lecture */



typedef struct tagLANGANDCP
{
  WORD wLanguage;
  WORD wCodePage;
} LANGANDCP;


#ifdef __cplusplus
    /* Compatibilit� avec un programme appelant �crit en C++ */
    #define _COMP_CPP   "C"
    #define _EXTERN     extern
#else
    #define _COMP_CPP
    #define _EXTERN
#endif


/* D�clarations des fonctions de la biblioth�que API de Lecture */
#ifdef _API_LEC
	#define _DLL_PTR
#else
	#ifdef API_ENTRY
		#undef API_ENTRY
	#endif
    #if defined(UNIX) || defined(TARGET_API_MAC_OSX)
		#define API_ENTRY	/* API*/
    	#define _DLL_PTR
    #else
		#define API_ENTRY	API
    	#define _DLL_PTR	*
	#endif
#endif

#if defined(UNIX) || defined(TARGET_API_MAC_OSX)
#define API_LEC_EXPORT API_ENTRY
#else
#define API_LEC_EXPORT API_IMPORT_EXPORT API
#endif

/********************************************************************/
/* Types des fonctions HAUT NIVEAU de la biblioth�que API de Lecture */
/********************************************************************/
typedef unsigned short  (API_ENTRY  _DLL_PTR T_Hn_Init)
                                (   IN  unsigned char  FARPTR	 pcChemin,
									OUT unsigned short FARPTR    pusMode,
									OUT unsigned short FARPTR    pusCodeErreur );

typedef unsigned short  (API_ENTRY  _DLL_PTR T_Hn_Finir)
                                (   OUT unsigned short FARPTR    pusCodeErreur );


typedef unsigned short  (API_ENTRY _DLL_PTR T_Hn_LectureVitale)
								(	IN      short                   sTempsAttente,
									OUT     char FARPTR				pcDataOut,
									IN_OUT	unsigned int FARPTR   puiLgDataOut,
									OUT		short FARPTR			psEtatCarte,
                                    OUT     unsigned short FARPTR   pusCodeErreur);

typedef unsigned short  (API_ENTRY _DLL_PTR T_Hn_IntroductionMstCPS)

								(	IN		short					sTempsAttente,
									OUT		char 					tcCodeCivilite[2],
									OUT		char					szNomPatronymique[27],
									OUT		char FARPTR				pcPresentPinCode,
									OUT		short FARPTR			psEtatCarte,
									OUT		unsigned short FARPTR	pusCodeErreur);


typedef unsigned short  (API_ENTRY _DLL_PTR T_Hn_RetraitMhtCPS)
								(	IN		short					sTempsAttente,
									OUT		unsigned short FARPTR	pusCodeErreur);


typedef unsigned short  (API_ENTRY _DLL_PTR T_Hn_ControleCPS)
							    (   IN		char					tcPinCode[4],
								    OUT		char					tcDateFinValidite[8],
								    OUT		char					tcTypeCarte[2],
								    OUT		char FARPTR				pcPresentPinCode,
								    OUT		char					tcCategorie[2],
								    OUT		unsigned short FARPTR	pusCodeErreur);

typedef unsigned short  (API_ENTRY  _DLL_PTR T_Hn_TestPresence)
                                (   IN  unsigned short          usCoupleur,
                                    OUT unsigned char FARPTR    pucStatut,
									OUT unsigned short FARPTR   pusCodeErreur);

/********************************************************************/
/* Types des fonctions BAS NIVEAU de la biblioth�que API de Lecture */
/********************************************************************/
typedef unsigned short  (API_ENTRY _DLL_PTR T_Lec_OuvertureSessionVitale)
                                        (   IN		unsigned char FARPTR    pcChemin,
											IN      char FARPTR             szNomAppli,
                                            OUT     unsigned short FARPTR   pusNoSession,
                                            OUT     unsigned short FARPTR   pusCodeErreur);

typedef unsigned short  (API_ENTRY _DLL_PTR T_Lec_FermetureSessionVitale)
                                        (   IN      unsigned short          usNoSession,
                                            OUT     unsigned short FARPTR   pusCodeErreur);

typedef unsigned short  (API_ENTRY _DLL_PTR T_Lec_IntroductionMstVitale)
                                        (   IN      unsigned short          usNoSession,
                                            IN      short                   sTempsAttente,
                                            OUT     short FARPTR            psEtatCarte,
                                            OUT     unsigned short FARPTR   pusCodeErreur);

typedef unsigned short  (API_ENTRY _DLL_PTR T_Lec_RetraitMhtVitale)
                                        (   IN      unsigned short          usNoSession,
                                            IN      short                   sTempsAttente,
                                            OUT     unsigned short FARPTR   pusCodeErreur);

typedef unsigned short  (API_ENTRY _DLL_PTR T_Lec_LectureVitale)
										(	IN		unsigned short			usNoSession,
											IN		unsigned short			usNoSessionCPS,
											OUT		char FARPTR				pcDataOut,
											IN_OUT	unsigned int FARPTR	puiLgDataOut,
											OUT		unsigned short FARPTR	pusCodeErreur);

typedef unsigned short  (API_ENTRY _DLL_PTR T_Lec_OuvertureSessionCPS)
										(	IN		unsigned char FARPTR    pcChemin,
											IN		char FARPTR				szNomAppli,
											OUT		unsigned short FARPTR	pusNoSession,
											OUT		unsigned short FARPTR	pusCodeErreur);

typedef unsigned short  (API_ENTRY _DLL_PTR T_Lec_FermetureSessionCPS)
										(	IN		unsigned short			usNoSession,
											OUT		unsigned short FARPTR	pusCodeErreur);

typedef unsigned short  (API_ENTRY _DLL_PTR T_Lec_IntroductionMstCPS)
										(	IN		unsigned short			usNoSession,
											IN		short					sTempsAttente,
											OUT		char 					tcCodeCivilite[2],
											OUT		char					szNomPatronymique[27],
											OUT		short FARPTR			psEtatCarte,
											OUT		unsigned short FARPTR	pusCodeErreur);

typedef unsigned short  (API_ENTRY _DLL_PTR T_Lec_RetraitMhtCPS)
										(	IN		unsigned short			usNoSession,
											IN		short					sTempsAttente,
											OUT		unsigned short FARPTR	pusCodeErreur);

typedef unsigned short  (API_ENTRY _DLL_PTR T_Lec_ControleCPS)
										(	IN		unsigned short			usNoSession,
											IN		char					tcPinCode[4],
											OUT		char					tcDateFinValidite[8],
											OUT		char					szIdentApplication[10],
											OUT		char					tcTypeCarte[2],
											OUT		char FARPTR				pcPresentPinCode,
											OUT		char					tcCategorie[2],
											OUT		unsigned short FARPTR	pusCodeErreur);

typedef unsigned short  (API_ENTRY _DLL_PTR T_Lec_Dictionnaire)
										(	IN		unsigned char FARPTR    pcChemin,
											IN		short					sNoTable,
											IN		char					tcCode[9],
											IN		char					tcPeriode[50],
											OUT		char FARPTR				szLibelle,
											IN_OUT	short FARPTR			psLgLibelle,
											OUT		unsigned short FARPTR	pusCodeErreur);

typedef unsigned short  (API_ENTRY _DLL_PTR T_Lec_InformationsAPI)
                                        (   IN		unsigned char FARPTR    pcChemin,
											IN_OUT  unsigned short FARPTR   pusLgInformations,
                                            OUT     char FARPTR             szInformations,
                                            OUT     unsigned short FARPTR   pusCodeErreur);

#if !defined(APILEC_EXPORTS)
/*********************************************************************************************/
/* D�claration des pointeurs sur les fonctions HAUT NIVEAU de la biblioth�que API de Lecture */
/*********************************************************************************************/
_EXTERN _COMP_CPP T_Hn_Init					Hn_Init;
_EXTERN _COMP_CPP T_Hn_Finir				Hn_Finir;
_EXTERN _COMP_CPP T_Hn_LectureVitale		Hn_LectureVitale;
_EXTERN _COMP_CPP T_Hn_IntroductionMstCPS	Hn_IntroductionMstCPS;
_EXTERN _COMP_CPP T_Hn_RetraitMhtCPS		Hn_RetraitMhtCPS;
_EXTERN _COMP_CPP T_Hn_ControleCPS			Hn_ControleCPS;
_EXTERN _COMP_CPP T_Hn_TestPresence			Hn_TestPresence;


/*********************************************************************************************/
/* D�claration des pointeurs sur les fonctions BAS NIVEAU de la biblioth�que API de Lecture */
/*********************************************************************************************/
_EXTERN _COMP_CPP T_Lec_OuvertureSessionVitale		Lec_OuvertureSessionVitale;
_EXTERN _COMP_CPP T_Lec_FermetureSessionVitale		Lec_FermetureSessionVitale;
_EXTERN _COMP_CPP T_Lec_IntroductionMstVitale		Lec_IntroductionMstVitale;
_EXTERN _COMP_CPP T_Lec_RetraitMhtVitale			Lec_RetraitMhtVitale;
_EXTERN _COMP_CPP T_Lec_LectureVitale				Lec_LectureVitale;
_EXTERN _COMP_CPP T_Lec_OuvertureSessionCPS			Lec_OuvertureSessionCPS;
_EXTERN _COMP_CPP T_Lec_FermetureSessionCPS			Lec_FermetureSessionCPS;
_EXTERN _COMP_CPP T_Lec_IntroductionMstCPS			Lec_IntroductionMstCPS;
_EXTERN _COMP_CPP T_Lec_RetraitMhtCPS				Lec_RetraitMhtCPS;
_EXTERN _COMP_CPP T_Lec_ControleCPS					Lec_ControleCPS;
_EXTERN _COMP_CPP T_Lec_Dictionnaire				Lec_Dictionnaire;
_EXTERN _COMP_CPP T_Lec_InformationsAPI			    Lec_InformationsAPI;
#endif

#undef _DLL_PTR

/*
--------------------------------------------------------------------------------------------------
*/
#endif

