/*
%-------------------------------------------------------------------------------------------------
% PROJET    : API LECTURE VITALE
%
% MODULE    : LEC
%
% VERSION   : 1.00
%
% FICHIER   : Api_Err.h
%
%   D�finitions des codes retourn�s par les fonctions des API de lecture V4
%
%-------------------------------------------------------------------------------------------------
% Version 1.00  - JLD - 07/06/1999 -
%-----------------------------------
%
%   Cr�ation
%
*/

#ifndef __LEC_H_ERR__
#define __LEC_H_ERR__

/* Erreurs */

#define ERR_INCONNUE		    10      /* erreur inconnue */
#define ERR_CARTE_ABSENTE		11
#define ERR_CARTE				12		/* erreur rendue par la carte */
#define ERR_LECTEUR				13		/* erreur rendue par le lecteur */
#define ERR_CARTE_INCONNUE		15
#define ERR_CARTE_MUETTE		17
#define ERR_LOGICIEL			18
#define ERR_GALSS				20
#define ERR_SESSION_OUVERTE		21		/* session deja ouverte */
#define ERR_EXCLUSIVITE			23		/* exclusivite en cours */
#define	ERR_TIMEOUT				25		/* Temporisation �coul�e */
#define ERR_SESSION_INCONNUE	26		/* session inconnue */
#define ERR_CPS_RETIREE			30		/* CPS retir�e ou chang�e */
#define ERR_VALIDITE_CPS		36		/* CPS ne permettant pas la lecture de carte Vitale */
#define ERR_CARTE_INCORRECTE	37		/* Donn�es en carte non exploitables */
#define ERR_CARTE_INVALIDE		38		/* La carte est invalid�e */
#define ERR_CARTE_BLOQUEE		39
#define ERR_CARTE_SATUREE		40
#define ERR_PINCODE_INACTIF		41		/* Pin code CPS non actif, contr�le impossible */
#define	ERR_CARTE_TEST			42		/* Types de Carte CPS et Vitale non concordant */
#define ERR_VERSION_APICPS      46      /* Version API GIP trop faible */
#define ERR_APICPS				50      /* Erreur retourn�e par les API GIP-CPS */
#define ERR_APICPS_NON_CHARGEE  51      /* Fonction CPS non disponible(API non charg�e)*/
#define ERR_PARAM_NULL			52		/* Le param�tre pass� est null */
#define ERR_PARAM_ADRESSE		53		/* Le param�tre pass� a la m�me adresse qu'un autre param�tre */
#define	ERR_PARAM_VALEUR		54		/* Le param�tre pass� a une valeur incorrecte */
#define ERR_PARAM_TAILLE		55		/* Le param�tre pass� n'a pas une taille suffisante */

/* Erreurs li�es au fichier de configuration api_lec.ini */
#define ERR_INIT_FICHIER		32		/* Fichier de configuration introuvable */
#define ERR_INIT_MODE			33		/* Mode de fonctionnement incorrect dans le fichier de configuration */
#define ERR_INIT_TIMER			34		/* Valeur timer incorrecte dans le fichier de configuration */

/* Erreurs li�es au fichier table binaire tablebin.lec */
#define ERR_INIT_TABLE			47		/* Table binaire introuvable */
#define ERR_VERSION_TABLE       45      /* Probl�me de version de la table */
#define ERR_STRUCTURE_TABLE     44      /* Probl�me de structure de la table binaire */

/* Erreurs li�es au fichier csv pdt-cdc-011.csv */
#define ERR_INIT_CSV			43      /* Fichier csv introuvable */
#define ERR_STRUCTURE_CSV		48		/* Probl�me de structure du fichier csv */	

/* Erreurs li�es au fichier des habilitations tablebin.hab */
#define ERR_INIT_HAB			56		/* Fichier non trouv�	*/
#define ERR_VERSION_HAB			57		/* Version incorrecte	*/
#define ERR_STRUCTURE_HAB		58		/* Pb structure fichier	*/

/* Erreurs li�es au fichier sedica.ini */
#define ERR_INIT_SEDICA			59		/* Fichier sedica.ini non trouv� */

/* Erreurs timer (Flip Flop) */
#define ERR_TIMER_CTRL			60		/* Timer de contr�le �coul� (Mode Flip-Flop) */
#define ERR_TIMER_INAC			61		/* Timer d'inactivit� �coul� (Mode Flip-Flop) */

/* Warnings */
#define WAR_APICPS_NON_DISPONIBLE 102	/* LEs API du gip ne sont pas disponible				*/
#define WAR_VERSION_APICPS      103		/* La version des API GIP n'est pas correcte			*/
#define WAR_DONNEES_ADM			104		/* Seules les donn�es administratives sont retourn�es	*/


/* Module d'origine de l'erreur (Valeur CodeErreur pour erreur ERR_LOGICIEL) */
#define MODULE_LEC              1       /* module origine de l'erreur logicielle */
#define MODULE_GALSS            2       /* module origine de l'erreur logicielle */
#define MODULE_CPS              3       /* module origine de l'erreur logicielle */


/* Codes compl�mentaires Erreurs carte */
#define ERR_CARTE_ME1_ME2		0x1009
#define ERR_CARTE_VIERGE		0x100A
#define ERR_CARTE_PLEINE		0x100B
#define ERR_CARTE_COMPAC		0x100C
#define ERR_CARTE_INEXISTANTE	0x100D


#endif
