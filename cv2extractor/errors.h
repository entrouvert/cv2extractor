/* $Id$
 *
 * cv2extractor - A free implementation of a reader of Carte Vitale 2 cards.
 *
 * Copyright (C) 2015 Entr'ouvert
 * https://dev.entrouvert.org/projects/cv2extractor
 *
 * Authors: See AUTHORS file in top-level directory.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/


/**
 * CV2EXTRACTOR_NO_ERROR:
 *
 * No error.
 */
#define CV2EXTRACTOR_NO_ERROR 0
/**
 * CV2EXTRACTOR_ERROR_UNDEFINED:
 *
 * Undefined error.
 */
#define CV2EXTRACTOR_ERROR_UNDEFINED -1
/**
 * CV2EXTRACTOR_ERROR_UNIMPLEMENTED:
 *
 * Unimplemented part of Cv2extractor.
 */
#define CV2EXTRACTOR_ERROR_UNIMPLEMENTED -2
/**
 * CV2EXTRACTOR_ERROR_OUT_OF_MEMORY:
 *
 * Out of memory.
 */
#define CV2EXTRACTOR_ERROR_OUT_OF_MEMORY -3
/**
 * CV2EXTRACTOR_ERROR_CAST_FAILED:
 *
 * Expected GObject class was not found, cast failed.
 */
#define CV2EXTRACTOR_ERROR_CAST_FAILED -4
/**
 * CV2EXTRACTOR_ERROR_CAST_FAILED:
 *
 * Error executing ssl command.
 */
#define CV2EXTRACTOR_ERROR_SSH_COMMAND -5
/**
 * CV2EXTRACTOR_ERROR_OPENING_FILE:
 *
 * Error opening a file.
 */
#define CV2EXTRACTOR_ERROR_OPENING_FILE -6
/**
 * CV2EXTRACTOR_ERROR_WRITING_FILE:
 *
 * Error writing a file.
 */
#define CV2EXTRACTOR_ERROR_WRITING_FILE -7
/**
 * CV2EXTRACTOR_ERROR_CLOSING_FILE:
 *
 * Error closing a file.
 */
#define CV2EXTRACTOR_ERROR_CLOSING_FILE -8


