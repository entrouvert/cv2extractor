/* $Id$
 *
 * cv2extractor - A free implementation of a reader of Carte Vitale 2 cards.
 *
 * Copyright (C) 2015 Entr'ouvert
 * https://dev.entrouvert.org/projects/cv2extractor
 *
 * Authors: See AUTHORS file in top-level directory.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <time.h>

#include <glib.h>
#include <glib-object.h>
#include <glib/gprintf.h>

#include "api_lec.h"
#include "api_err.h"

#include "config.h"
#include "errors.h"
#include "utils.h"
#include "cJSON.h"

int main(){
    g_type_init();

    int rc = CV2EXTRACTOR_ERROR_UNDEFINED;

    g_printf("---------------------------------------\n");
    g_printf("----- cv2extractor Dict extractor -----\n");
    g_printf("---------------------------------------\n\n");


    /* Function functionnal error code */
    unsigned short pusCodeErreur = 0;

    /* Function execution error code */
    unsigned short err = 0;

    // Erreur 18/2, faire tourner TestAl et tester le dic.
    char tcCode[9];
    char tcPeriode[50];
    tcPeriode[0] = '\0';
    char* szLibelle = (char*) malloc(2048);
    short psLgLibelle = 2048;

    cJSON *root, *table;
    root=cJSON_CreateObject();

    int i, j, k, l;
    //TABLE_QUALITE
    printf("TABLE_QUALITE\n");
    cJSON_AddItemToObject(root, "TABLE_QUALITE", table=cJSON_CreateObject());
    for (i=0; i<10; i++){
        tcCode[0] = '0' + i;
        err = Lec_Dictionnaire(NULL, TABLE_QUALITE, tcCode, tcPeriode, szLibelle,
                       &psLgLibelle, &pusCodeErreur);
        if (err == 0) {
            printf("%s\n", szLibelle);
            cJSON_AddStringToObject(table, tcCode, szLibelle);
        }
        psLgLibelle = 2048;
    }

    //TABLE_DIVERS
    printf("TABLE_DIVERS\n");
    cJSON_AddItemToObject(root, "TABLE_DIVERS", table=cJSON_CreateObject());
    for (i=0; i<10; i++){
        tcCode[0] = '0' + i;
        err = Lec_Dictionnaire(NULL, TABLE_DIVERS, tcCode, tcPeriode, szLibelle,
                       &psLgLibelle, &pusCodeErreur);
        if (err == 0) {
            printf("%s\n", szLibelle);
            cJSON_AddStringToObject(table, tcCode, szLibelle);
        }
        psLgLibelle = 2048;
    }

    //TABLE_SERVAMO
    printf("TABLE_SERVAMO\n");
    cJSON_AddItemToObject(root, "TABLE_SERVAMO", table=cJSON_CreateObject());
    for (i=0; i<10; i++){
        tcCode[0] = '0' + i;
        err = Lec_Dictionnaire(NULL, TABLE_SERVAMO, tcCode, tcPeriode, szLibelle,
                       &psLgLibelle, &pusCodeErreur);
        if (err == 0) {
            printf("%s\n", szLibelle);
            cJSON_AddStringToObject(table, tcCode, szLibelle);
        }
        psLgLibelle = 2048;
    }

    //TABLE_SERVMUTAMC
    printf("TABLE_SERVMUTAMC\n");
    cJSON_AddItemToObject(root, "TABLE_SERVMUTAMC", table=cJSON_CreateObject());
    for (i=0; i<10; i++){
        tcCode[0] = '0' + i;
        err = Lec_Dictionnaire(NULL, TABLE_SERVMUTAMC, tcCode, tcPeriode, szLibelle,
                       &psLgLibelle, &pusCodeErreur);
        if (err == 0) {
            printf("%s\n", szLibelle);
            cJSON_AddStringToObject(table, tcCode, szLibelle);
        }
        psLgLibelle = 2048;
    }

    //TABLE_CMU
    printf("TABLE_CMU\n");
    cJSON_AddItemToObject(root, "TABLE_CMU", table=cJSON_CreateObject());
    for (i=0; i<10; i++){
        tcCode[0] = '0' + i;
        for (j=0; j<10; j++){
            tcCode[1] = '0' + j;
            err = Lec_Dictionnaire(NULL, TABLE_CMU, tcCode, tcPeriode, szLibelle,
                           &psLgLibelle, &pusCodeErreur);
            if (err == 0) {
                printf("%s\n", szLibelle);
                cJSON_AddStringToObject(table, tcCode, szLibelle);
            }
            psLgLibelle = 2048;
        }
    }

    //TABLE_GESTION
    printf("TABLE_GESTION\n");
    cJSON_AddItemToObject(root, "TABLE_GESTION", table=cJSON_CreateObject());
    for (i=0; i<10; i++){
        tcCode[0] = '0' + i;
        for (j=0; j<10; j++){
            tcCode[1] = '0' + j;
            err = Lec_Dictionnaire(NULL, TABLE_GESTION, tcCode, tcPeriode, szLibelle,
                           &psLgLibelle, &pusCodeErreur);
            if (err == 0) {
                printf("%s\n", szLibelle);
                cJSON_AddStringToObject(table, tcCode, szLibelle);
            }
            psLgLibelle = 2048;
        }
    }

    //TABLE_REGIME
    printf("TABLE_REGIME\n");
    cJSON_AddItemToObject(root, "TABLE_REGIME", table=cJSON_CreateObject());
    for (i=0; i<10; i++){
        tcCode[0] = '0' + i;
        for (j=0; j<10; j++){
            tcCode[1] = '0' + j;
            err = Lec_Dictionnaire(NULL, TABLE_REGIME, tcCode, tcPeriode, szLibelle,
                           &psLgLibelle, &pusCodeErreur);
            if (err == 0) {
                printf("%s\n", szLibelle);
                cJSON_AddStringToObject(table, tcCode, szLibelle);
            }
            psLgLibelle = 2048;
        }
    }

    //TABLE_INDICMUT
    printf("TABLE_INDICMUT\n");
    cJSON_AddItemToObject(root, "TABLE_INDICMUT", table=cJSON_CreateObject());
    for (i=0; i<10; i++){
        tcCode[0] = '0' + i;
        for (j=0; j<10; j++){
            tcCode[1] = '0' + j;
            err = Lec_Dictionnaire(NULL, TABLE_INDICMUT, tcCode, tcPeriode, szLibelle,
                           &psLgLibelle, &pusCodeErreur);
            if (err == 0) {
                printf("%s\n", szLibelle);
                cJSON_AddStringToObject(table, tcCode, szLibelle);
            }
            psLgLibelle = 2048;
        }
    }

    //TABLE_INDICAMC
    printf("TABLE_INDICAMC\n");
    cJSON_AddItemToObject(root, "TABLE_INDICAMC", table=cJSON_CreateObject());
    for (i=0; i<10; i++){
        tcCode[0] = '0' + i;
        for (j=0; j<10; j++){
            tcCode[1] = '0' + j;
            err = Lec_Dictionnaire(NULL, TABLE_INDICAMC, tcCode, tcPeriode, szLibelle,
                           &psLgLibelle, &pusCodeErreur);
            if (err == 0) {
                printf("%s\n", szLibelle);
                cJSON_AddStringToObject(table, tcCode, szLibelle);
            }
            psLgLibelle = 2048;
        }
    }

    //TABLE_CODEROUT
    printf("TABLE_CODEROUT\n");
    cJSON_AddItemToObject(root, "TABLE_CODEROUT", table=cJSON_CreateObject());
    for (i=0; i<10; i++){
        tcCode[0] = '0' + i;
        for (j=0; j<10; j++){
            tcCode[1] = '0' + j;
            err = Lec_Dictionnaire(NULL, TABLE_CODEROUT, tcCode, tcPeriode, szLibelle,
                           &psLgLibelle, &pusCodeErreur);
            if (err == 0) {
                printf("%s\n", szLibelle);
                cJSON_AddStringToObject(table, tcCode, szLibelle);
            }
            psLgLibelle = 2048;
        }
    }

    //TABLE_CAISSE
    printf("TABLE_CAISSE\n");
    cJSON_AddItemToObject(root, "TABLE_CAISSE", table=cJSON_CreateObject());
    for (i=0; i<1; i++){
        tcCode[0] = '0' + i;
        for (j=0; j<10; j++){
            tcCode[1] = '0' + j;
            for (k=0; k<10; k++){
                tcCode[2] = '0' + k;
                for (l=0; l<10; l++){
                    tcCode[3] = '0' + l;
                    err = Lec_Dictionnaire(NULL, TABLE_CAISSE, tcCode, tcPeriode, szLibelle,
                                   &psLgLibelle, &pusCodeErreur);
                    if (err == 0) {
                        printf("%s\n", szLibelle);
                        cJSON_AddStringToObject(table, tcCode, szLibelle);
                    }
                    psLgLibelle = 2048;
                }
            }
        }
    }

    char *out;
    out = cJSON_Print(root);
    cJSON_Delete(root);
    printf("%s\n",out);

    FILE *f = fopen("tables.json", "w");
    fprintf(f, "%s", out);
    fclose(f);

    free(out);

    rc = CV2EXTRACTOR_NO_ERROR;

cleanup:
    /* Cleaning... */
    return rc;
}
