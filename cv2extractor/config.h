/* $Id$
 *
 * cv2extractor - A free implementation of a reader of Carte Vitale 2 cards.
 *
 * Copyright (C) 2015 Entr'ouvert
 * https://dev.entrouvert.org/projects/cv2extractor
 *
 * Authors: See AUTHORS file in top-level directory.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* Major version number */
#define CV2EXTRACTOR_VERSION_MAJOR 0

/* Minor version number */
#define CV2EXTRACTOR_VERSION_MINOR 0

/* Release version number */
#define CV2EXTRACTOR_VERSION_SUBMINOR 1

/* Time between each card presence test */
#define TIME_SLEEP 1

/* Size of memory allocated for CarteVitale reading */
#define BUFFER_SIZE 500000

/* Number of digits of a PIN Number */
#define PIN_DIGITS 4

/* Number of PIN entry tries */
#define PIN_TRIES 3

/* Waiting time for CPS removal */
#define CPS_WAITING_TIME 3600

/* Path to write XML files */
#define XML_FILES_PATH "."

/* Prefix to xml filenames */
#define XML_FILE_PREFIX "reader1_" // e.g. "reader1_"

/* ssh host target, including username, empty means no ssh transfer */
#define SSL_TRANSFER_TARGET ""

/* ssh passphrase */
#define SSL_TRANSFER_SECRET ""

/* CPS Pin code - Avoid interactive mode */
#define CPS_PIN_CODE ""
